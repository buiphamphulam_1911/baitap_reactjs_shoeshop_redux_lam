import { dataShoe } from "../../data_shoe";
import {
  CONG,
  THEM_VAO_GIO,
  TRU,
  XEM_CHI_TIET,
  XOA,
} from "../constants/shoeConstants";

let initialState = {
  shoeArr: dataShoe,
  detailShoe: dataShoe[0],
  gioHang: [],
};
// rxreducer
export let shoeReducer = (state = initialState, action) => {
  // console.log("action.payload: ", action.payload);
  console.log("state: ", state);
  switch (action.type) {
    case XEM_CHI_TIET: {
      state.detailShoe = action.payload;
      return { ...state };
    }
    case THEM_VAO_GIO: {
      let index = state.gioHang.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index != -1) {
        state.gioHang[index].soLuong++;
        state.gioHang = [...state.gioHang];
      } else {
        state.gioHang = [...state.gioHang, action.payload];
      }
      return { ...state };
    }
    case XOA: {
      let index = state.gioHang.findIndex((item) => {
        return item.id == action.payload;
      });
      state.gioHang[index].soLuong = 1;
      state.gioHang.splice(index, 1);
      state.gioHang = [...state.gioHang];
      return { ...state };
    }
    case CONG: {
      let index = state.gioHang.findIndex((item) => {
        return item.id == action.payload;
      });
      state.gioHang[index].soLuong++;
      state.gioHang = [...state.gioHang];
      return { ...state };
    }
    case TRU: {
      let index = state.gioHang.findIndex((item) => {
        return item.id == action.payload;
      });
      if (state.gioHang[index].soLuong == 1) {
        state.gioHang.splice(index, 1);
        state.gioHang = [...state.gioHang];
      } else {
        state.gioHang[index].soLuong--;
        state.gioHang = [...state.gioHang];
      }
      return { ...state };
    }
    default:
      return state;
  }
};
