import React, { Component } from "react";
import { connect } from "react-redux";
import { dataShoe } from "./data_shoe";
import DetailShoe from "./DetailShoe";
import GioHang from "./GioHang";
import ListShoe from "./ListShoe";

export class Ex_ShoeShopRedux extends Component {
  render() {
    return (
      <div>
        <GioHang />
        <ListShoe />
        <DetailShoe />
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.gioHang,
    detailShoe: state.shoeReducer.detailShoe,
  };
};

export default connect(mapStateToProps, null)(Ex_ShoeShopRedux);
