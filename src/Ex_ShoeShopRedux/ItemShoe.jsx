import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { THEM_VAO_GIO, XEM_CHI_TIET } from "./redux/constants/shoeConstants";

class ItemShoe extends Component {
  render() {
    return this.props.shoeArr.map((item) => {
      return (
        <Fragment>
          <div className="col-3">
            <div className="card" style={{ width: "18rem" }}>
              <img
                className="card-img-top"
                src={item.image}
                alt="Card image cap"
              />
              <div className="card-body">
                <h5 className="card-title">{item.name}</h5>
                <p className="card-text">
                {item.description}
                </p>
                <p className="card-text">
                {item.price}$
                </p>
                <button
                  onClick={() => {
                    this.props.handleAddToCart(item);
                  }}
                  className="btn btn-secondary"
                >
                  Add to cart
                </button>

                <button
                  onClick={() => {
                    this.props.handleViewDetail(item);
                  }}
                  className="btn btn-warning"
                >
                  Xem chi tiết
                </button>
              </div>
            </div>
          </div>
        </Fragment>
      );
    });
  }
}
let mapStateToProps = (state) => {
  return {
    shoeArr: state.shoeReducer.shoeArr,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleViewDetail: (value) => {
      dispatch({
        type: XEM_CHI_TIET,
        payload: value,
      });
    },
    handleAddToCart: (value) => {
      dispatch({
        type: THEM_VAO_GIO,
        payload: value,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ItemShoe);
