import React, { Component } from "react";
import { connect } from "react-redux";
import { CONG, TRU, XOA } from "./redux/constants/shoeConstants";

export class GioHang extends Component {
  renderTbody = () => {
    if (this.props.gioHang.length != 0) {
      return this.props.gioHang.map((item) => {
        return (
          <tr>
            <td>{item.name}</td>
            <td>{item.price}$</td>
            <td>
              {" "}
              <img src={item.image} style={{ width: 80 }} alt="" />
            </td>
            <td>
              <button
                className="btn btn-primary"
                onClick={() => {
                  this.props.handleTru(item.id);
                }}
              >
                -
              </button>
              <span className="mx-3">{item.soLuong}</span>
              <button
                className="btn btn-success"
                onClick={() => {
                  this.props.handleCong(item.id);
                }}
              >
                +
              </button>
             
            </td>
            <td>
            <button
               
               onClick={() => {
                 this.props.handleRemove(item.id);
               }}
               className="btn btn-danger mx-3"
             >
               Xoá
             </button>
            </td>
          </tr>
        );
      });
    }
  };

  render() {
    return (
      <div className="container py-5">
        {/* gioHang */}
        <table className="table text-left">
          <thead>
            <tr>
              <th>Tên</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
              <th>Số lượng</th>
              <th>Hành Động</th>
            </tr>
          </thead>

          <tbody>{this.renderTbody()}</tbody>
        </table>

        {this.props.gioHang.length == 0 && (
          <p className=" mt-5 text-center">Chưa có sản phẩm trong giỏi hàng</p>
        )}
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.gioHang,
  };
};
let mapDispathToProps = (dispatch) => {
  return {
    handleRemove: (value) => {
      dispatch({
        type: XOA,
        payload: value,
      });
    },
    handleCong: (value) => {
      dispatch({
        type: CONG,
        payload: value,
      });
    },
    handleTru: (value) => {
      dispatch({
        type: TRU,
        payload: value,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispathToProps)(GioHang);
