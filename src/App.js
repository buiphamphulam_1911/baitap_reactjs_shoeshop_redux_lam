import logo from "./logo.svg";
import "./App.css";


import Ex_ShoeShopRedux from "./Ex_ShoeShopRedux/Ex_ShoeShopRedux";



function App() {
  return (
    <div className="App">
      {/* <DemoClass></DemoClass> */}

      {/* <DemoFunction /> */}
      {/* <Ex_Layout /> */}

      {/* <Data_Binding /> */}
      {/* <Event_Binding /> */}
      {/* <Conditional_Rendering /> */}
      {/* <Demo_State /> */}
      {/* <RenderWithMap /> */}
      {/* <Demo_Prop /> */}
      {/* <Ex_ShoeShop /> */}
      {/* <BaiTapLayoutComponent /> */}
      {/* <DemoReduxMini /> */}
      <Ex_ShoeShopRedux />
      {/* <Ex_Tai_Xiu /> */}
      {/* <Demo_LifeCycle /> */}
      {/* <Ex_User_Management /> */}
    </div>
  );
}

export default App;
